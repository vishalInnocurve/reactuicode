import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
class AuthHeader extends React.Component {
  render() {
    if (this.props.isAuthenticated === true) {
      console.log("AuthHeader");
      return <div>hello there i'm check component</div>;
    } else {
      return <Redirect push to="/" />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
  };
};

export default connect(mapStateToProps)(AuthHeader);
