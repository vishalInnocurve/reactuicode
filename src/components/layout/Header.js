import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Auth } from "aws-amplify";

import { loginActionUser, isAuthenticated } from "../../actions";

class Header extends React.Component {
  onClick = async (e) => {
    console.log("singout");
    console.log("e", e);
    try {
      const signoutData = await Auth.signOut();
      console.log("singout", signoutData);
      this.props.isAuthenticatedAction(false);
      this.props.loginActionUser(null);
    } catch (e) {
      console.log("e", e);
    }
  };
  render() {
    console.log("check check", this.props.isAuthenticated);
    if (this.props.isAuthenticated === false) {
      return (
        <div className="ui secondary  menu">
          <Link to="/" className="active item">
            Home
          </Link>
          <Link to="/" className="item">
            Messages
          </Link>
          <div className="right menu">
            <Link to="/" className="ui item">
              LogIn
            </Link>
            <Link to="/register" className="ui item">
              SignUp
            </Link>
          </div>
        </div>
      );
    } else {
      return (
        <div className="ui secondary  menu">
          <Link to="/" className="active item">
            Home
          </Link>
          <Link to="/" className="item">
            Messages
          </Link>
          <div className="right menu">
            <button onClick={(e) => this.onClick(e)} className="ui item">
              Logout
            </button>
          </div>
        </div>
      );
    }
  }
}
const mapStateToProps = (state) => {
  console.log("state", state);
  return {
    isAuthenticated: state.isAuthenticated,
  };
};
export default connect(mapStateToProps, {
  loginActionUser,
  isAuthenticatedAction: isAuthenticated,
})(Header);
