import { BrowserRouter, Route } from "react-router-dom";
import React from "react";
import Header from "./layout/Header";
import Login from "./auth/Login";
import Register from "./auth/SignUp";
import Welcome from "./Welcome";
import AuthHeader from "./layout/AuthHeader";
import { connect } from "react-redux";
import { loginActionUser, isAuthenticated } from "../actions";
import { Auth } from "aws-amplify";
class App extends React.Component {
  state = { isAuthenticating: true };

  async componentDidMount() {
    console.log("main componentDidMount");
    try {
      const session = await Auth.currentSession();
      console.log("session", session);
      this.props.isAuthenticatedAction(true);
      const user = await Auth.currentAuthenticatedUser();
      this.props.loginActionUser(user);
    } catch (e) {
      console.log("error while retrying the user login", e);
      this.props.loginActionUser(null);
      this.props.isAuthenticatedAction(false);
    }
    this.setState({ isAuthenticating: false });
  }
  render() {
    if (this.state.isAuthenticating === false) {
      console.log("Inside App Component");
      return (
        <div>
          <BrowserRouter>
            <Header></Header>
            <Route exact path="/" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/welcome" component={Welcome} />
            <Route exact path="/check" component={AuthHeader} />
            App
          </BrowserRouter>
        </div>
      );
    } else {
      console.log("WAITING WAITING");
      return null;
    }
  }
}
const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
  };
};
export default connect(mapStateToProps, {
  loginActionUser,
  isAuthenticatedAction: isAuthenticated,
})(App);
