import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
class Welcome extends React.Component {
  state = { isRedirect: null };
  onClick = (e) => {
    this.setState({ isRedirect: true });
  };
  render() {
    console.log("welcome props before everything", this.props);
    if (this.state.isRedirect) {
      return <Redirect push to="/check" />;
    } else if (this.props.isAuthenticated === true) {
      console.log("Welcome");
      return (
        <div>
          Welcome to Student
          <button onClick={(e) => this.onClick(e)}>Check</button>
        </div>
      );
    } else {
      return <Redirect push to="/" />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
  };
};

export default connect(mapStateToProps)(Welcome);
