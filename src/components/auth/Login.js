import React, { useState, Fragment, useEffect } from "react";
import { Auth } from "aws-amplify";
import { connect } from "react-redux";
import { loginActionUser, isAuthenticated } from "../../actions";
import { Redirect } from "react-router-dom";

const Login = (props) => {
  console.log(props);
  const [isAuth, setIsAuth] = useState(false);

  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });

  useEffect(() => {
    if (isAuth) {
      props.isAuthenticated(true);
      return <Redirect push to="/welcome" />;
    }
  });
  if (isAuth) {
    return <Redirect push to="/welcome" />;
  }
  const { username, password } = formData;
  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    console.log("formData", formData);

    try {
      let { username, password } = formData;
      const user = await Auth.signIn(username, password);
      console.log("user logged in", user);
      //this.props.isAuthenticated(true);
      //this.props.loginActionUser(user);
      setIsAuth(true);
    } catch (e) {
      console.log("error while signIN", e);
    }
  };
  return (
    <Fragment>
      <div>LOGIN FORM</div>
      <form className="ui form" onSubmit={(e) => onSubmit(e)}>
        <div className="field">
          <label>UserName</label>
          <input
            type="text"
            name="username"
            placeholder="username"
            value={username}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="field">
          <label>Password</label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={(e) => onChange(e)}
          />
        </div>
        <button className="ui button" type="submit">
          Submit
        </button>
      </form>
    </Fragment>
  );
};

//const mapStateToProps=()=>{}

export default connect(null, { loginActionUser, isAuthenticated })(Login);
