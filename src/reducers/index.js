import { combineReducers } from "redux";
const isAuthenticated = function (isAuth = false, action) {
  if (action.type === "IS_AUTHENTICATED") {
    return action.payload;
  }
  return isAuth;
};

const userData = function (user = null, action) {
  if (action.type === "USER_DATA") {
    return action.payload;
  }
  return user;
};
export default combineReducers({
  isAuthenticated,
  userData,
});
