export const loginActionUser = (user) => {
  return {
    type: "USER_DATA",
    payload: user,
  };
};

export const isAuthenticated = (isAuthenticated) => {
  return {
    type: "IS_AUTHENTICATED",
    payload: isAuthenticated,
  };
};
